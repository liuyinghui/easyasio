set(base_SRCS
	base/format.cpp
	base/logger.cpp
	base/timestamp.cpp
  )

set(net_SRCS
	net/io_service_pool.cpp  
	net/tcp_client.cpp  
	net/tcp_connection.cpp  
	net/tcp_server.cpp
  )

add_library(easyasio ${base_SRCS} ${net_SRCS})
#target_link_libraries(akcp boost_system boost_thread)

add_subdirectory(base/test)

#define  _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#include <vector>
#include <iostream>

#include "base/logger.h"
#include "base/utils.hpp"

#define THREAD_NUM 1
#define MSG_NUM 5000000

using namespace easyasio::base;

void write_log(CountDownLatch& finish)
{
    easyasio::base::Timer t;
    int num = MSG_NUM / THREAD_NUM;
    for (int i = 0; i < num; ++i)
        LOG_TRACE("{}", "hello world !");
    std::cout << "thread-->" <<  t.elapsed() << std::endl;
    finish.countDown();
}

int main(int argc, char* argv[])
{
    CountDownLatch finish(THREAD_NUM);
    std::shared_ptr<std::thread> ts[THREAD_NUM];

    Logger::instance().setBaseName("./log1/logger_test");

    {
        std::cout << "start test" << std::endl;
        Timer t;
        for (int i = 0; i < THREAD_NUM; ++i)
            ts[i].reset(new std::thread(std::bind(write_log, std::ref(finish))));
        finish.wait();
        std::cout << "total cost--------------->: "<< t.elapsed() << std::endl;
    }

    for (auto& item : ts)
        if (item->joinable())
            item->join();

    char c = getchar();
    return 0;
}
#include "tcp_connection.hpp"

#include <vector>
#include <functional>
#include <iostream>
#include <atomic>
#include "../base/logger.h"

using namespace std::placeholders;

namespace easyasio {
namespace net {

TcpConnection::TcpConnection(asio::io_service& io_service)
	: socket_(io_service),
	  loop_(io_service),
	  state_(Connecting)
{
}

TcpConnection::~TcpConnection()
{
    LOG_TRACE("TcpConnection destory. {}", (void*)this);
}

asio::ip::tcp::socket& TcpConnection::socket()
{
	return socket_;
}

void TcpConnection::start()
{
    assert(state() == Connecting);
    setState(Connected);

    LOG_TRACE("net::TcpConnection accept, ip {}, port {}. {}", 
        socket_.remote_endpoint().address().to_string(), socket_.remote_endpoint().port(), (void*)this);

    if (connectionCallback_)
    {
        loop_.dispatch(std::bind(connectionCallback_, shared_from_this()));
    }
    recv_buffer_.ensureWritableBytes(RECV_CACHED_SIZE);
	socket_.async_read_some(asio::buffer(recv_buffer_.beginWrite(), recv_buffer_.writableBytes()), 
		bind(&TcpConnection::handleReadableInLoop, shared_from_this(), std::placeholders::_1, std::placeholders::_2));
}

void TcpConnection::handleReadableInLoop(const asio::error_code& e, std::size_t bytes_transferred)
{
	if (e)
	{
        if (!permitSendInLoop())
        {
            LOG_INFO("net::TcpConnection::handleReadableInLoop invalide state {}: {}:{}. {}", 
                    state(), e.value(), e.message(), (void*)this);
            return;
        }

        LOG_INFO("net::TcpConnection::handleReadableInLoop error: {}:{}. {}", e.value(), e.message(), (void*)this);
        forceCloseInloop();
		return; 
	}

    if (state()==Connected)
    {
        recv_buffer_.hasWritten(bytes_transferred);
    	if (messageCallback_)
    		messageCallback_(shared_from_this(), &recv_buffer_);

        recv_buffer_.ensureWritableBytes(RECV_CACHED_SIZE);
    	socket_.async_read_some(asio::buffer(recv_buffer_.beginWrite(), recv_buffer_.writableBytes()),
    		std::bind(&TcpConnection::handleReadableInLoop, shared_from_this(), _1, _2));
    }
}

void TcpConnection::handleWritableInLoop(const asio::error_code& e)
{
	if (e)
	{
        if (!permitSendInLoop())
        {
            //assert(0);
            LOG_INFO("net::TcpConnection::handleWritableInLoop invalide state {}: {}:{}. {}", 
                    state(), e.value(), e.message(), (void*)this);
            return;
        }

        LOG_INFO("net::TcpConnection::handleWritableInLoop: {}:{}. {}", e.value(), e.message(), (void*)this);
        forceCloseInloop();
        return;
	}

	sending_buffer_.retrieveAll();
    if (permitSendInLoop())
    {
    	socketTrySendInloop();
    }
}

void TcpConnection::socketTrySendInloop()
{
    if (pending_send_buffer_.readableBytes()==0)
    {
        if (!isWriting() && state()==Disconnecting) 
        { 
            shutdownInLoop();
        }
		return;
    }

    if (sending_buffer_.readableBytes() > 0)
        return;

	sending_buffer_.swap(pending_send_buffer_);
	
    asio::async_write(socket_,
          asio::buffer(sending_buffer_.peek(), sending_buffer_.readableBytes()),
          std::bind(&TcpConnection::handleWritableInLoop, shared_from_this(), std::placeholders::_1));
}

void TcpConnection::send(const std::string& msg)
{
    if (state() != Connected)
    {
        LOG_WARN("net::TcpConnection::send state != Connected, state={}, giving up send. {}", state(), (void*)this);
        return;
    }

	loop_.post(std::bind(&TcpConnection::sendInLoop, shared_from_this(), msg));
}

void TcpConnection::sendInLoop(const std::string& msg)
{
    if (!permitSendInLoop())
    {
        LOG_INFO("net::TcpConnection::sendInLoop state = {}. {}", state(), (void*)this);
        return;
    }

	pending_send_buffer_.append(msg.c_str(), msg.size());
    socketTrySendInloop();
}

void TcpConnection::shutdown()
{
    if (state() != Connected)
    {
        assert(0);
        LOG_WARN("net::TcpConnection::shutdown state invalied = {}. {}", state(), (void*)this);
        return;
    }
    LOG_DEBUG("net::TcpConnection::shutdown. {}", (void*)this);
    setState(Disconnecting);
    
    loop_.post(std::bind(&TcpConnection::shutdownInLoop, shared_from_this()));
}

void TcpConnection::shutdownInLoop()
{
    if (pending_send_buffer_.readableBytes()==0 && 
        !isWriting() && 
        state()==Disconnecting)
    {
        setState(Disconnected);
        LOG_TRACE("net::TcpConnection::shutdownInLoop. {}", (void*)this);
        //不能用shutdown, 否则极端情况下被关闭端有可能收不到FIN
        socket_.cancel();
        if (closeCallback_)
            closeCallback_(shared_from_this());
    }
    else
    {
        LOG_INFO("net::TcpConnection::shutdownInLoop {} {} {}. {}", 
            pending_send_buffer_.readableBytes()==0, !isWriting(), state()==Disconnecting, (void*)this);
        //socketTrySendInloop();
    }
}

void TcpConnection::forceClose()
{
    if (!permitSendInLoop())
    {
        LOG_WARN("net::TcpConnection::forceClose state invalied = {}. {}", state(), (void*)this);
        return;
    }

    LOG_DEBUG("net::TcpConnection::forceClose. {}", (void*)this);
    setState(forceclosing);
    
    loop_.dispatch(std::bind(&TcpConnection::forceCloseInloop, shared_from_this()));
}

void TcpConnection::forceCloseInloop() 
{ 
    handleCloseInLoop();
}

void TcpConnection::handleCloseInLoop()
{
    setState(Disconnected);
    LOG_TRACE("net::TcpConnection close socket. {}", (void*)this);
    socket_.cancel();

    if (closeCallback_)
        closeCallback_(shared_from_this());
}

} // namespace net 
} // namespace easyasio 

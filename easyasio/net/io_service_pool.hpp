#ifndef EASYASIO_NET_IO_SERVICE_POOL_HPP 
#define EASYASIO_NET_IO_SERVICE_POOL_HPP 

#include <vector>
#include <memory>

#include <asio.hpp>

#include "../base/utils.hpp"

namespace easyasio {
using namespace base;
namespace net {

class io_service_pool
	: private noncopyable
{
public:
	explicit io_service_pool(std::size_t pool_size, bool usemultiloop = true);

	void run() ;

	void stop() ;

	asio::io_service& get_io_service() ;

private:
	typedef std::shared_ptr<asio::io_service> io_service_ptr;
	typedef std::shared_ptr<asio::io_service::work> work_ptr;
	std::vector<io_service_ptr> io_services_;
	std::vector<work_ptr> work_;
	std::size_t next_io_service_;

    bool usemultiloop_;
    std::size_t pool_size_;
};

}//net
}//easyasio

#endif // EASYASIO_NET_IO_SERVICE_POOL_HPP 

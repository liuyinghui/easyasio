1 生成：
tar -xvf boost_1_64_0.tar.gz
cd boost_1_64_0
./bootstrap.sh
mkdir miniboost
./b2 tools/bcp
./dist/bin/bcp --namespace=mnb --namespace-alias interprocess miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias dll miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias algorithm miniboost 
./dist/bin/bcp --namespace=mnb --namespace-alias assert miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias program_options miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias date_time miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias crc miniboost
./dist/bin/bcp --namespace=mnb --namespace-alias uuid miniboost 

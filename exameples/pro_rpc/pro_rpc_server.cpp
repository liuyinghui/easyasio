#include "echo.pb.h"

#include <functional>
#include <iostream>

#include "base/logger.h"
#include "protorpc/rpc_server.hpp"

namespace echo
{

class EchoServiceImpl : public EchoService
{
public:
    virtual void Echo(::google::protobuf::RpcController* controller,
                    const ::echo::EchoRequest* request,
                    ::echo::EchoResponse* response,
                    ::google::protobuf::Closure* done)
    {
        //LOG_INFO << "EchoServiceImpl::Solve";
        response->set_payload(request->payload());
        done->Run();
    }
};


}

int main()
{
    echo::EchoServiceImpl impl;
    easyasio::net::RpcServer server("127.0.0.1", "1234", 1);
    server.registerService(&impl);
    server.start();

    int x;
    std::cin >> x;
}


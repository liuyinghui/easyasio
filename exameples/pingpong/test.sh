#!/bin/sh

killall server
timeout=${timeout:-30}
bufsize=16384
ulimit  -a

for nosessions in 10000; do
  for nothreads in 1; do
    sleep 5
    echo "Bufsize: $bufsize Threads: $nothreads Sessions: $nosessions"
    ./pingpong_server 0.0.0.0 55555 $nothreads & srvpid=$!
    ./pingpong_client 127.0.0.1 55555 $nothreads $bufsize $nosessions $timeout
    kill -9 $srvpid
  done
done

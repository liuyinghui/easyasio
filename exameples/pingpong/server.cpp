#include <utility>
#include <stdio.h>
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <atomic>
#include <iostream>
#include "net/tcp_server.hpp"
#include "net/io_service_pool.hpp"
#include "base/logger.h"

using namespace easyasio::net;
void onConnection(const TcpConnectionPtr& conn)
{
    if (conn->connected())
    {
        //conn->setTcpNoDelay(true);
    }
}

void onMessage(const TcpConnectionPtr& conn, Buffer* buf)
{
    conn->send(std::string(buf->peek(), buf->readableBytes()));
    buf->retrieveAll();
}

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        fprintf(stderr, "Usage: server <address> <port> <threads>\n");
    }
    else
    {
        easyasio::base::Logger::instance().setLevel(easyasio::base::Logger::LOGLEVEL_WARN);
        int threadCount = atoi(argv[3]);

        std::shared_ptr<TcpServer> server = std::make_shared<TcpServer>(std::string(argv[1]), std::string(argv[2]), threadCount);

        server->setConnectionCallback(onConnection);
        server->setMessageCallback(onMessage);
        server->run();

    	int a;
    	std::cin >> a;
    }
}


include_directories(../../easyasio)

add_executable(correctnesstest_server correctnesstest_server.cpp)
target_link_libraries(correctnesstest_server easyasio pthread miniboost)

add_executable(correctnesstest_client correctnesstest_client.cpp)
target_link_libraries(correctnesstest_client easyasio pthread miniboost)

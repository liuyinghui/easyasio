#include "net/udp_endpoint.h"
#include <iostream>

void messageCallback(const UdpEndpointPtr& endpoint, const asio::ip::udp::endpoint& remote, const char* buf, size_t len)
{
    std::cout << "server recv: " << remote.address().to_string() << ": " << remote.port() << "msg: " << std::string(buf, len) << std::endl;
    //std::cout << "server recv: " << std::string(buf, len) << std::endl;
    endpoint->sendto(std::string(buf, len), "239.255.0.1", 4567);
}

int main(int argc, char* argv[])
{
    asio::io_service loop;
    UdpEndpointPtr server = std::make_shared<UdpEndpoint>(loop, "0.0.0.0", 4567);
    server->setMessageCallback(messageCallback);
    server->join_multicast_group("239.255.0.1");
    server->start();

    loop.run();

    return 0;
}